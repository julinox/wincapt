#pragma once

/****************************************
 * High funcionality class so to speak	*
 * Does not perform any allocation for	*
 * taks simplicity sake.				*
 ****************************************/

 /* Macros */
#define CTOKENS std::list<char*>
#define WTOKENS std::list<wchar_t*>
#define CTOKENSIT CTOKENS::iterator
#define WTOKENSIT WTOKENS::iterator

/* Includes */
#include <list>

class JCString
{
public:

	/* Attributes */
	char CharTrim;
	wchar_t WcharTrim;

	/* Methods */
	JCString();
	JCString(char trimC);
	JCString(wchar_t trimW);
	~JCString();

	size_t GetSize(const char *str);
	size_t GetSize(const wchar_t *str);
	
	/* Trimming */
	char *TrimL(char *src);
	wchar_t *TrimL(wchar_t *src);
	void TrimR(char *src);
	void TrimR(wchar_t *src);
	void Trim(char *dst, size_t dstLen, char *src);
	void Trim(wchar_t *dst, size_t dstLen, wchar_t *src);
	
	void Tokenize(CTOKENS *dst, char *src, const char *dlm);
	void Tokenize(WTOKENS *dst, wchar_t *src, const wchar_t *dlm);
	void CharToWchar(wchar_t *dst, char *src);
	void PrintTokens(CTOKENS*);
	void PrintTokens(WTOKENS*);
	void ReleaseTokens(CTOKENS*);
	void ReleaseTokens(WTOKENS*);

	/* Private */

private:
};

