#pragma once

/* Macros */
#define LOGPATHBUFSZ 256

/* Enums & Structs */
enum LOGGER_ERRS {
	ERR_ARGV_ANY = 0xBB,
	ERR_ARGV_INPUT = 0xE0,
	ERR_ARGV_FLAG = 0xE1,
};

class WC_Logger
{
public:
	/* Attributes */

	/* Methods */
	WC_Logger(char*);
	~WC_Logger();
	void LogErr(int, void*);
	void LogErr(int, const wchar_t*, ...);
	void Log(const wchar_t *, ...);
	void Log(const char *, ...);

private:
	/* Attributes */
	FILE *LogFile;

	/* Methods */
	void LogTime();
};

