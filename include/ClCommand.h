#pragma once

/* Macros */

/* Own includes */
#include "ClDefs.h"

/* Enums & Structs */
enum CONTROL_ACTION {
	ACTION_PRESS = 0xA0,
	ACTION_CHECK = 0xB0,
	ACTION_UNCHECK = 0xB1,
	ACTION_CHECK_RB = 0xC1,
	ACTION_UNCHECK_RB = 0xC2
};

class ClCommand
{
public:
	//HWND parent;

	ClCommand();
	~ClCommand();
	void CmdAction(CTRL_DATA*);
	void CmdClick(CTRL_DATA*);
	void CmdText(CTRL_DATA*);
	void ClPause(int);

private:

};

