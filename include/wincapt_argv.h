#pragma once

/********************************************************************
 * Verbose: Prints extra info										*
 * Daemon Mode: "Autonomous" mode (Must have inpunt file)			*
 * Default Mode: "Guided" mode										*
 * InputFile: Input file name for "autonomous mode"					*
 * OutputFile: Output file name for "guided mode"					*
 *																	*
 * For autonomous mode:												*
 * Poll Time: Try to capture FIRST window every "poll time"			*
 * Kill Time: If not first window captured then exit at "kill time"	*
 ********************************************************************/

/* Macros */
#define INPUTBUFFSZ 256
#define INVALIDFLAGS 10
#define DEF_LOGGERFILENAME "stdout"

/* Enums & Structs */
enum ARGV_OP_MODE {
	ARGV_MODE_UNSET = 0xA0,
	ARGV_MODE_AUTONOMOUS = 0xAD,
	ARGV_MODE_AUTONOMOUS_D = 0xAE,
	ARGV_MODE_GUIDED = 0xAF
};

class WC_Argv
{
public:
	/* Attributes */
	int Mode;
	int Matches;
	bool Verbose;
	int PollTime;
	int ExitTime;
	char Invalid[INVALIDFLAGS];
	char InputFile[INPUTBUFFSZ];
	char OutputFile[INPUTBUFFSZ];
	wchar_t KeyWords[INPUTBUFFSZ];
	char LoggerFileName[INPUTBUFFSZ];

	/* Methods */
	WC_Argv();
	~WC_Argv();
	void PrintHelp(bool);
	void PrintCmds();
	bool CmdL(int, char**);

private:
	/* Attributes */
	int Flags;

	/* Methods */
	void PrintErr(int err);
	void PrintAsciiArt();
};