#pragma once

/* Enums & Structs */
typedef struct {
	STEP_H others;
	char loggerFname[INPUTBUFFSZ];
	wchar_t keywords[INPUTBUFFSZ];
} HEADER;

typedef struct {
	size_t actualStep;
	u_int actualWinClassDist;
	
	/* Classes */
	WC_Name *Name;
	WC_Logger *Logger;

} S_PARAMS;

class WC_Autonomous
{
public:
	/* Attributes */
	
	/* Methods */
	WC_Autonomous();
	~WC_Autonomous();
	void Run(WC_Argv*);

private:
	/* Attributes */
	HEADER Header;
	WC_Step *Steps;
	WC_Logger *Logger;

	/* Methods */
	bool ReadInfo(char*);
	void ReadHeader(std::ifstream&);
	bool ReadSteps(std::ifstream&);
	HANDLE SetExitTimer(int);
	bool __Capture__(S_PARAMS*, STEPIT);
	void __QuickFix__(char*);
};

