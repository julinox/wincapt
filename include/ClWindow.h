#pragma once

/* Macros */
#define CLASS_NUMBERS 4

/* Includes */

/* C style includes */

/* C++ style Includes */
#include <list>

/* Own includes */
#include "ClDefs.h"

/* Enums & Structs */
enum PRINT_FLAGS {
	FIELD_ID = BIT 0,
	FIELD_HWND = BIT 1,
	FIELD_FLAGS = BIT 2,
	FIELD_CNAME = BIT 3,
	FIELD_SNAME = BIT 4,
	FIELD_NAME = BIT 5,
	FIELD_GLOBALID = BIT 6
};

class ClWindow
{
public:
	/* Attributes */
	int ctrlSz;
	u_int winClassDist;
	std::list<CTRL_DATA> ctrls;

	/* Methods */
	ClWindow();
	~ClWindow();
	void CtrlsCapture(HWND);
	void WinCountClassDist();
	void CtrlsPrint(int, int);
	void CtrlsFill(HWND);
	void CtrlsLogErr(const char*, DWORD);
	CTRL_DATA *CtrlsGetById(int);
	CTRL_DATA *CtrlsGetByGlobalId(long);
	void CtrlsPrint_(CTRL_DATA*, int);
	void CtrlsSetFlags(CTRL_DATA*);
	void CtrlSetText(HWND, WCHAR*);

private:
	/* Attributes */

	/* Methods */
};

