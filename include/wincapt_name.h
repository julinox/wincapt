#pragma once

/* Macros */

/* Enums & Structs */

/* C style includes */

/* C++ style includes */
//#include <list>

class WC_Name
{
public:
	/* Attributes */
	HWND ActHwnd;
	size_t MinSz;
	WTOKENS Tokens;
	WC_Logger *Logger;
	
	/* Methods */
	WC_Name(wchar_t*, size_t, WC_Logger*);
	~WC_Name();
	void FindWindowCustom();
	bool CBHelper1(wchar_t*);

private:
	/* Attributes */
	size_t Matches;

	/* Methods */
	void SetMinSz();
};