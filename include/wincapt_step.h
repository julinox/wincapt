#pragma once

/* MACROS */
#define _TMPBUFSZ_ 256
#define STEPIT std::list<_STEP_*>::iterator

/* Includes */

/* Own includes */
#include "ClDefs.h"

/* Enums & Structs */
typedef struct {
	int step;
	CTRL_DATA ctrl;
	u_int winClassDist;
} _STEP_;

typedef struct {
	int matches;
	int pollTime;
	int exitTime;
} STEP_H;

class WC_Step
{
public:
	/* Attributes */
	std::list<_STEP_*> Steps;

	/* Methods */
	WC_Step(char*);
	~WC_Step();
	void Push(CTRL_DATA*, u_int, int);
	void WriteInfo(char*, WTOKENS&, STEP_H*);
	void DeleteLastStep();
	void PrintSteps();
	void QuickFix(WC_Logger*);

private:
	/* Attributes */
	FILE *FilePtr;
	char *FileName;
	WC_Logger *Logger;

	/* Methods */
	void OpenFile(bool);
	void WriteHeading();
	void WriteKeywords(WTOKENS&);
	void WriteLoggerFname(char*);
	void WriteOthers(STEP_H*);
	void WriteSteps();
};

