#pragma once
/* Enums & Structs */

#ifdef  _WC_GUIDED_RPARAMS_
	typedef struct {
		int loop;
		int stepNmb;
		bool doWrite;
		int noNameSz;
		HWND actHwnd;
		int noName[TOUCHCTRLS];

		/* Classes */
		WC_Name *name;
		WC_Step *steps;
		ClWindow *window;

		/* 'External' Classes */
		WC_Argv *argvv;
		WC_Logger *logger;
	} R_PARAMS;

#endif //

class WC_Guided
{
public:
	/* Attributes */

	/* Methods */
	WC_Guided();
	~WC_Guided();
	void Run(WC_Argv*, WC_Logger*);
	void SetDlm(std::string);

private:
	/* Attributes */
	std::string Delimiter;

	/* Methods */
#ifdef _WC_GUIDED_PRIVATE_
	void __Capture__(R_PARAMS*);
	void __Print__(R_PARAMS*);
	void __Read__(R_PARAMS*);
	void __Save__(R_PARAMS*);
#endif

};
