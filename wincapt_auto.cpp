/* Windows-related shit */
#include "pch.h"

/* Macros */
#define _SECOND_MS_ 1000
#define _SECOND_ 10000000LL
#define _SLEEP_IF_(x,y) if (x) { x = false; } else { Sleep(_SECOND_MS_ * y); }
//#define _SLEEP_(x,y) if (x) { x = false; } else { Sleep(y); }
#define TRY_CATCH_STEP(x,y) try { x = y; } catch (...) \
							{ this->Logger->Log("Input File Err: Abort!"); return (false); }
#define SKIP_BLANKS_COMMENTS	if (str.length() == 0) continue; \
								if (str.at(0) == '#') continue;

/* Includes */
//#include <list>
#include <string>
#include <fstream>
#include <iostream>
#include <windows.h>

/* Own includes */
#include "include/wincapt_strs.h"
#include "include/wincapt_logger.h"
#include "include/wincapt_argv.h"
#include "include/wincapt_name.h"
#include "include/ClWindow.h"
#include "include/ClCommand.h"
#include "include/wincapt_step.h"
#include "include/wincapt_auto.h"

/* Enums & Structs */

/* Glocals */
static int LOOPING = 1;

/* Prototypes */

/* Definitions */
WC_Autonomous::WC_Autonomous()
{
	this->Logger = NULL;
	this->Steps = new WC_Step(NULL);
	memset(&this->Header, 0, sizeof(HEADER));
}

WC_Autonomous::~WC_Autonomous()
{
	if (this->Steps != NULL)
		delete(this->Steps);
}

void WC_Autonomous::Run(WC_Argv *argvv)
{
	STEPIT it;
	HANDLE eTimer;
	bool right, ft;
	S_PARAMS sParams;
	ClCommand command;
	
	ft = true;
	memset(&sParams, 0, sizeof(S_PARAMS));
	right = this->ReadInfo(argvv->InputFile);
	
	if (!right)
		goto _DEALLOC_;
	if (this->Steps->Steps.size() < 1)
		goto _DEALLOC_;

	sParams.Logger = new WC_Logger(this->Header.loggerFname);
	this->Logger = sParams.Logger;
	eTimer = this->SetExitTimer(this->Header.others.exitTime);
	if (eTimer == NULL) {
		sParams.Logger->Log("Error exit timer(%d)", GetLastError());
		goto _DEALLOC_;
	}
	sParams.Name = new WC_Name(this->Header.keywords,
		this->Header.others.matches, sParams.Logger);
	
	it = this->Steps->Steps.begin();
	sParams.actualStep = (*it)->step;
	sParams.actualWinClassDist = (*it)->winClassDist;
	
	sParams.Logger->Log("Start");
	while (LOOPING) {
		_SLEEP_IF_(ft, this->Header.others.pollTime);
		if (WaitForSingleObject(eTimer, 0) == WAIT_OBJECT_0) {
			sParams.Logger->Log("TimeOut");
			LOOPING = 0;
			continue;
		}
		sParams.Name->FindWindowCustom();
		if (sParams.Name->ActHwnd == NULL)
			continue;
		if (!this->__Capture__(&sParams, it))
			continue;
		command.CmdAction(&(*it)->ctrl);
		sParams.Logger->Log(L"Exec: %s", (*it)->ctrl.ctlName);
		++it;
		if (it == this->Steps->Steps.end())
			break;
		if (sParams.actualStep != (*it)->step) {
			sParams.actualStep = (*it)->step;
			sParams.actualWinClassDist = (*it)->winClassDist;
		}
	}

	sParams.Logger->Log("End");
	_DEALLOC_:
	if (sParams.Name != NULL)
		delete(sParams.Name);
	if (sParams.Logger != NULL)
		delete(sParams.Logger);
}

bool WC_Autonomous::__Capture__(S_PARAMS *sParams, STEPIT it)
{
	ClWindow window;
	CTRL_DATA *ctrlX;

	window.CtrlsCapture(sParams->Name->ActHwnd);
	if (window.ctrls.size() < 1)
		return (false);
	
	if (window.winClassDist != sParams->actualWinClassDist) {
		std::cout << "NOT THE ACTUAL WINDOW\n";
		return (false);
	}

	ctrlX = window.CtrlsGetById((*it)->ctrl.ctlID);
	if (ctrlX == NULL) {
		ctrlX = window.CtrlsGetByGlobalId((*it)->ctrl.globalID);
		if (ctrlX == NULL)
			return (false);
	}

	/*if (ctrlX->ctlFlags != (*it)->ctrl.ctlFlags) {
		sParams->Logger->Log("No Flags: %d | %d", ctrlX->ctlFlags,
			(*it)->ctrl.ctlFlags);
	}*/
	if (ctrlX->ctlFlags & CLASS_EDIT) {
		if (!((*it)->ctrl.ctlFlags & CLASS_EDIT))
			return (false);

	} else if (_wcsicmp((*it)->ctrl.ctlName, ctrlX->ctlName)) {
		return (false);
	}
	(*it)->ctrl.ctlHwnd = ctrlX->ctlHwnd;
	return (true);
}

bool WC_Autonomous::ReadInfo(char *inputFile)
{
	std::ifstream fileSt(inputFile);

	if (fileSt.fail()) {
		this->__QuickFix__(inputFile);
		return (false);

	}
	this->ReadHeader(fileSt);
	return (this->ReadSteps(fileSt));
}

void WC_Autonomous::ReadHeader(std::ifstream &ifSt)
{
	size_t i;
	JCString jcs;
	CTOKENS auxCT;
	std::string str;
	char cAuxBuf[INPUTBUFFSZ];

	i = 0;
	memset(cAuxBuf, 0, INPUTBUFFSZ);
	while (std::getline(ifSt, str) && i < 3)
	{
		SKIP_BLANKS_COMMENTS
		if (i == 0) {
			jcs.Trim(cAuxBuf, INPUTBUFFSZ, (char *)str.c_str());
			jcs.CharToWchar(this->Header.keywords, cAuxBuf);
			i++;
			continue;
		}
		if (i == 1) {
			jcs.Trim(cAuxBuf, INPUTBUFFSZ, (char *)str.c_str());
			strcpy_s(this->Header.loggerFname, INPUTBUFFSZ, cAuxBuf);
			i++;
			continue;
		}
		if (i == 2)
			break;	
	}
	jcs.Tokenize(&auxCT, (char *)str.c_str(), " ");

	i = 0;
	for (CTOKENSIT it = auxCT.begin(); it != auxCT.end(); ++it) {
		try {
			switch (i++) {
			case 0:
				this->Header.others.matches = std::stoi(*it);
				break;
			case 1:
				this->Header.others.pollTime = std::stoi(*it);
				break;
			case 2:
				this->Header.others.exitTime = std::stoi(*it);
				break;
			}
		}
		catch (...) { ; }
	}
	jcs.ReleaseTokens(&auxCT);
}

bool WC_Autonomous::ReadSteps(std::ifstream& ifSt)
{
	JCString jcs;
	size_t count;
	_STEP_ newStep;	
	std::string str;

	count = 0;
	while (std::getline(ifSt, str))
	{
		SKIP_BLANKS_COMMENTS
		if (str == "<STEP>") {
			memset(&newStep, 0, sizeof(_STEP_));
			continue;
		} else if (str == "</STEP>") {
			continue;
		}
		
		switch (count) {
		case 0:
			TRY_CATCH_STEP(newStep.step, std::stoi(str))
			break;
		case 1:
			TRY_CATCH_STEP(newStep.ctrl.ctlID, std::stoi(str))
			break;
		case 2:
			TRY_CATCH_STEP(newStep.ctrl.globalID, std::stoi(str))
			break;
		case 3:
			TRY_CATCH_STEP(newStep.ctrl.ctlFlags , std::stoi(str))
			break;
		case 4:
			jcs.CharToWchar(newStep.ctrl.ctlName, (char *)str.c_str());
			break;
		case 5:
			TRY_CATCH_STEP(newStep.winClassDist , std::stoi(str))
			break;
		}
		count++;
		if (count > 5) {
			count = 0;
			this->Steps->Push(&newStep.ctrl, newStep.winClassDist, newStep.step);
		}
	}
	return (true);
}

HANDLE WC_Autonomous::SetExitTimer(int seconds)
{
	HANDLE hTimer = NULL;
	LARGE_INTEGER liDueTime;

	liDueTime.QuadPart = (-_SECOND_) * seconds;
	hTimer = CreateWaitableTimer(NULL, TRUE, NULL);
	if (hTimer == NULL)
		return NULL;
	if (!SetWaitableTimer(hTimer, &liDueTime, 0, NULL, NULL, 0))
		return NULL;
	return (hTimer);
}

void WC_Autonomous::__QuickFix__(char *fileName)
{
	WC_Logger aux((char*)DEF_LOGGERFILENAME);
	aux.Log("File Not Found: %s", fileName);
}