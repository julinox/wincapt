/* C style includes */
#include "pch.h"
#include <string.h>
#include <windows.h>

/* C++ style includes */
#include <map>
#include <list>
#include <string>
#include <iomanip>
#include <iostream>

/* Own includes */
#include "include/ClWindow.h"
#include <inttypes.h>
/* Macros */
#define PRIVATE static
#define IS_BUTTON L"button"
#define IS_STATIC L"static"
#define IS_EDIT L"edit"
#define CLASS_BUTTON_N	"CLASS_BUTTON"
#define CLASS_EDIT_N	"CLASS_EDITBX"
#define CLASS_STATIC_N	"CLASS_STATIC"
#define CLASS_UNKNOW_N	"CLASS_UNKNOW"
#define ITERATOR std::list<CTRL_DATA>::iterator
#define SLEEP(x) printf("Sleeping...\n"); Sleep(x)

/* Glocals */
PRIVATE const char EditName[] = "TEXTBOX";
PRIVATE const struct _btn_ {
	int btnStyle;
	char btnText[CTRL_NAME_SZ];
} BTNS[] = {
	BS_PUSHBUTTON,"PUSHBUTTON",
	BS_DEFPUSHBUTTON, "PUSHBUTTON-DEF",
	BS_USERBUTTON, "USERBUTTON",
	BS_CHECKBOX, "CHECKBOX",
	BS_AUTOCHECKBOX, "CHECKBOX-AUTO",
	BS_3STATE, "CHECKBOX_3STATE",
	BS_AUTO3STATE, "CHECKBOX_AUTO-3STATE",
	BS_RADIOBUTTON, "RADIOBUTTON",
	BS_AUTORADIOBUTTON, "RADIOBUTTON-AUTO",
	BS_PUSHBOX, "PUSHBOX",
	BS_OWNERDRAW, "OWNERDRAW"
};

/*
 * This is a redundant shit since i fucked up
 * with the conversion of Windows Style defined
 * values to custom defined windows style flags 
 * values. So simply put in other words this is
 * a "quickfick"
 */

std::map<int, int> QuickFix = {
	{BS_PUSHBUTTON, STYLE_PUSHBUTTON},
	{BS_DEFPUSHBUTTON, STYLE_DEFPUSHBUTTON},
	{BS_USERBUTTON, STYLE_USERBUTTON},
	{BS_CHECKBOX, STYLE_CHECKBOX},
	{BS_AUTOCHECKBOX, STYLE_AUTOCHECKBOX},
	{BS_3STATE, STYLE_3STATE},
	{BS_AUTO3STATE, STYLE_AUTO3STATE},
	{BS_RADIOBUTTON, STYLE_RADIO},
	{BS_AUTORADIOBUTTON, STYLE_AUTORADIO},
	{BS_PUSHBOX, STYLE_PUSHBOX},
	{BS_OWNERDRAW, STYLE_OWNERDRAW},
};

/* Local prototypes */
PRIVATE void CtrlsPrint_(CTRL_DATA*, int);
PRIVATE BOOL CALLBACK GetAllCtrls_(HWND, LPARAM);

/* Constructor & Destructor */
ClWindow::ClWindow()
{
	this->ctrlSz = 0;
	this->winClassDist = 0;
}

ClWindow::~ClWindow()
{
	this->ctrlSz = 0;
	this->ctrls.clear();
	this->winClassDist = 0;
}


/* **************** Public **************** */
void ClWindow::CtrlsCapture(HWND window)
{
	EnumChildWindows(window, GetAllCtrls_, (LPARAM)(void *)(this));
	this->WinCountClassDist();
}

void ClWindow::CtrlsFill(HWND hwnd)
{
	CTRL_DATA auxData;

	memset(&auxData, 0, sizeof(CTRL_DATA));
	auxData.ctlID = this->ctrlSz++;
	auxData.ctlHwnd = hwnd;
	auxData.globalID = GetWindowLong(hwnd, GWL_ID);
	this->CtrlSetText(hwnd, auxData.ctlName);
	this->CtrlsSetFlags(&auxData);
	this->ctrls.push_back(auxData);
}

void ClWindow::WinCountClassDist()
{
	/*
		0x| 00 | 00 | 00 | 00 |
		  |-cu-|-cs-|-ce-|-cb-|
	*/
	u_char c_b, c_e, c_s, c_u;

	c_b = c_e = c_s = c_u = 0;
	std::list<CTRL_DATA>::iterator it;

	it = this->ctrls.begin();
	for (; it != this->ctrls.end(); ++it) {
		if ((*it).ctlFlags & CLASS_BUTTON)
			c_b++;
		else if ((*it).ctlFlags & CLASS_EDIT)
			c_e++;
		else if ((*it).ctlFlags & CLASS_STATIC)
			c_s++;
		else if ((*it).ctlFlags & CLASS_UNKNOW)
			c_u++;
	}
	this->winClassDist |= c_b << (8 * 0);
	this->winClassDist |= c_e << (8 * 1);
	this->winClassDist |= c_s << (8 * 2);
	this->winClassDist |= c_u << (8 * 3);
}

void ClWindow::CtrlsPrint(int cType, int sType)
{
	ITERATOR it;

	it = this->ctrls.begin();

	printf("\n");
	if (sType & FIELD_ID)
		printf("|ID  |");
	if (sType & FIELD_HWND)
		printf("|HWND              |");
	if (sType & FIELD_GLOBALID)
		printf("|GID   |");
	if (sType & FIELD_FLAGS)
		printf("|Flags     |");
	if (sType & FIELD_CNAME)
		printf("|Class         |");
	if (sType & FIELD_SNAME)
		printf("|Type:                 |");
	if (sType & FIELD_NAME)
		printf("|Name             |");
	printf("\n\n");

	for (; it != this->ctrls.end(); ++it) {
		if ((*it).ctlFlags & cType)
			this->CtrlsPrint_(&*it, sType);
	}
	printf("\n");
}

CTRL_DATA *ClWindow::CtrlsGetById(int ctrlId)
{
	ITERATOR it;

	for (it = this->ctrls.begin(); it != this->ctrls.end(); ++it) {
		if (it->ctlID == ctrlId)
			return (&*it);
	}
	return (NULL);
}

CTRL_DATA *ClWindow::CtrlsGetByGlobalId(long ctrlGid)
{
	ITERATOR it;

	for (it = this->ctrls.begin(); it != this->ctrls.end(); ++it) {
		if (it->globalID == ctrlGid)
			return (&*it);
	}
	return (NULL);
}

/* **************** Private **************** */
void ClWindow::CtrlsSetFlags(CTRL_DATA *control)
{
	LONG gwl;
	DWORD ec;
	int it, stp;
	WCHAR auxBuf[CTRL_NAME_SZ];

	if (control == NULL)
		return;

	control->ctlFlags = 0;

	/* Setting class type */
	control->ctlFlags |= CLASS_ANY;
	memset(auxBuf, 0, CTRL_NAME_SZ);
	GetClassName(control->ctlHwnd, auxBuf, CTRL_NAME_SZ);

	if (!_wcsicmp(auxBuf, IS_BUTTON)) {
		control->ctlFlags |= CLASS_BUTTON;
		strncpy_s(control->ctlClassName, CLASS_BUTTON_N, sizeof(CLASS_BUTTON_N));

	}
	else if (!_wcsicmp(auxBuf, IS_EDIT)) {
		control->ctlFlags |= CLASS_EDIT;
		strncpy_s(control->ctlClassName, CLASS_EDIT_N, sizeof(CLASS_EDIT_N));
	} else if (!_wcsicmp(auxBuf, IS_STATIC)) {
		control->ctlFlags |= CLASS_STATIC;
		strncpy_s(control->ctlClassName, CLASS_STATIC_N, sizeof(CLASS_STATIC_N));

	} else {
		control->ctlFlags |= CLASS_UNKNOW;
		strncpy_s(control->ctlClassName, CLASS_UNKNOW_N, sizeof(CLASS_UNKNOW_N));
	}


	/* Setting style type */
	if (control->ctlFlags & CLASS_BUTTON) {
		control->ctlFlags |= STYLE_PUSHBUTTON;
		strcpy_s(control->ctlStyleName, CTRL_NAME_SZ, BTNS[0].btnText);
	} else if (control->ctlFlags & CLASS_EDIT) {
		control->ctlFlags |= STYLE_TEXT;
		strcpy_s(control->ctlStyleName, CTRL_NAME_SZ, EditName);
		return;
	} else {
		control->ctlFlags |= STYLE_UNDEF;
		strcpy_s(control->ctlStyleName, CTRL_NAME_SZ, "UNDEFINED");
		return;
	}

	gwl = GetWindowLong(control->ctlHwnd, GWL_STYLE) & BS_TYPEMASK;
	ec = GetLastError();
	if (!gwl && ec) {
		printf("GWL %p | clas: %s\n", control->ctlHwnd, control->ctlClassName);
		//this->CtrlsLogErr("GetWindowLong()", ec);
		return;
	}

	it = 0;
	stp = sizeof(BTNS) / sizeof(struct _btn_);
	while (it < stp) {
		if (gwl == BTNS[it].btnStyle) {
			//control->ctlFlags |= BTNS[it].btnStyle;
			control->ctlFlags |= QuickFix[BTNS[it].btnStyle];
			strcpy_s(control->ctlStyleName, CTRL_NAME_SZ, BTNS[it].btnText);
			//wprintf(L"%s:		", control->ctlName);
			//fff(control->ctlFlags);
			break;
		}
		++it;
	}
}

void ClWindow::CtrlSetText(HWND hwnd, WCHAR *buf)
{
	/* 
	 * WM_GETTEXT copies (into a given buffer) the text
	 * assigned to a control and returns the number of
	 * characters copied
	 *
	 * wParam -> Size of the buffer
	 * lParam -> Address of the buffer
	 */
	/*return (SendMessage(hwnd, WM_GETTEXT, (WPARAM)CTRL_NAME_SZ,
						(LPARAM)(void *)buf));*/
	SendMessage(hwnd, WM_GETTEXT, (WPARAM)CTRL_NAME_SZ,(LPARAM)(void *)buf);
}

void ClWindow::CtrlsLogErr(const char *msg, DWORD err)
{
	printf("%s\n", msg);
	printf("Error code: %d\n", err);
}

/* Auxiliary functions */
void ClWindow::CtrlsPrint_(CTRL_DATA *data, int pFlag)
{
	if (pFlag & FIELD_ID)
		printf("| %.2d |", data->ctlID);
	if (pFlag & FIELD_HWND)
		printf("| %p |", data->ctlHwnd);
	if (pFlag & FIELD_GLOBALID)
		printf("| %.4lu |", data->globalID);
	if (pFlag & FIELD_FLAGS)
		printf("| %.8d |", data->ctlFlags);
	if (pFlag & FIELD_CNAME)
		printf("| %s |", data->ctlClassName);
	if (pFlag & FIELD_SNAME)
		printf("| %20s |", data->ctlStyleName);
	if (pFlag & FIELD_NAME)
		wprintf(L"| %15.15s |\n", data->ctlName);
}

PRIVATE BOOL CALLBACK GetAllCtrls_(HWND cHwnd, LPARAM lParam)
{
	/*
	 * Callback for EnumChildWindows
	 * cHwnd: Handler (HWND) for the current child
	 */

	ClWindow *obj;

	obj = (ClWindow *)lParam;
	obj->CtrlsFill(cHwnd);
	return true;
}
