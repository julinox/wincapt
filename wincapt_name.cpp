/* Windows-Related shit */
#include "pch.h"

/* Macros */
#define BUFFSZ 256
#define __SPACE__ 2
#define DELIMITER L" "

/* includes */
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <algorithm>

#include <iostream>
#include <stdio.h>

/* Own includes */
#include "include/wincapt_logger.h"
#include "include/wincapt_strs.h"
#include "include/wincapt_name.h"

/* Prototypes */
static BOOL CALLBACK FindWindowCustomCB(HWND hwnd, LPARAM lp);

/* Definitions */
WC_Name::WC_Name(wchar_t *kws, size_t mchs, WC_Logger *logger)
{
	JCString jcs;

	this->MinSz = 0;
	this->ActHwnd = NULL;
	this->Matches = mchs;
	this->Logger = logger;
	jcs.Tokenize(&this->Tokens, kws, DELIMITER);
	this->SetMinSz();
	if (mchs == 0)
		this->Matches = this->Tokens.size();
}

WC_Name::~WC_Name()
{
	WTOKENSIT it;

	this->MinSz = 0;
	this->Logger = NULL;
	this->ActHwnd = NULL;
	it = this->Tokens.begin();
	
	for (; it != this->Tokens.end(); ++it)
		delete (*it);
	this->Tokens.clear();
}

void WC_Name::FindWindowCustom()
{
	this->ActHwnd = NULL;	
	EnumWindows(FindWindowCustomCB, LPARAM((void *)this));
}

static BOOL CALLBACK FindWindowCustomCB(HWND hwnd, LPARAM lp)
{
	bool _ret;
	size_t strSz;
	WC_Name *_this;
	wchar_t auxBuff[BUFFSZ];

	_ret = true;
	memset(auxBuff, 0, BUFFSZ * sizeof(wchar_t));
	if (!GetWindowText(hwnd, auxBuff, BUFFSZ))
		return (true);
	strSz = wcslen(auxBuff);
	_this = (WC_Name *)lp;
	if (strSz < _this->MinSz)
		return (true);
	_ret = _this->CBHelper1(auxBuff);
	if (!_ret) {
		_this->ActHwnd = hwnd;
	}
	return (_ret);
}

bool WC_Name::CBHelper1(wchar_t *pName)
{
	bool _ret;
	JCString jcs;
	size_t matches;
	WTOKENS tkList;
	WTOKENSIT it, pNames, wNames;

	_ret = true;
	matches = 0;
	
	jcs.Tokenize(&tkList, pName, DELIMITER);
	if (tkList.size() < this->Matches)
		goto _DEALLOC_;
	
	pNames = tkList.begin();	
	for (; pNames != tkList.end(); ++pNames) {
		wNames = this->Tokens.begin();
		for (; wNames != this->Tokens.end(); ++wNames) {
			if (!_wcsicmp(*wNames, *pNames)) {
				++matches;
				break;
			}
		}
		if (matches >= this->Matches) {
			_ret = false;
			goto _DEALLOC_;
		}
	}
	_DEALLOC_:
	it = tkList.begin();
	for (; it != tkList.end(); ++it)
		delete (*it);
	return (_ret);
}

void WC_Name::SetMinSz()
{
	WTOKENSIT it;
	size_t i, j, szArr, *minArr;

	i = 0;
	szArr = this->Tokens.size();
	minArr = new size_t[szArr];
	if (minArr == NULL)
		return;
	it = this->Tokens.begin();
	for (; it != this->Tokens.end(); ++it)
		minArr[i++] = wcslen(*it);
	std::sort(minArr, minArr + szArr);
	if (this->Matches == 0)
		j = szArr;
	else
		j = this->Matches;
	for (i = 0; i < j; i++)
		this->MinSz += minArr[i];
	delete (minArr);
}
