
/* C includes */
#include "pch.h"
#include <stdio.h>
#include <Windows.h>

/* C++ style includes */
#include <iostream>

/* Own includes */
#include "include/ClCommand.h"

/* Macros */

/* Definitions */
ClCommand::ClCommand()
{
}

ClCommand::~ClCommand()
{
}

void ClCommand::CmdAction(CTRL_DATA *ctrl)
{
	if (ctrl == NULL || ctrl->ctlHwnd == NULL)
		return;
	if (ctrl->ctlFlags & CLASS_EDIT)
		this->CmdText(ctrl);
	else if (ctrl->ctlFlags & CLASS_BUTTON)
		this->CmdClick(ctrl);
}

void ClCommand::CmdClick(CTRL_DATA *ctrl)
{
	HWND parent;

	parent = GetParent(ctrl->ctlHwnd);
	PostMessage(parent, WM_NEXTDLGCTL, (WPARAM)ctrl->ctlHwnd, TRUE);
	Sleep(FOCUSTIME);
	PostMessage(ctrl->ctlHwnd, WM_KEYDOWN, VK_SPACE, 0x29);
	PostMessage(ctrl->ctlHwnd, WM_KEYUP, VK_SPACE, 0x29);
	Sleep(STEPTIME);
}

void ClCommand::CmdText(CTRL_DATA *ctrl)
{
	HWND parent;

	parent = GetParent(ctrl->ctlHwnd);
	PostMessage(parent, WM_NEXTDLGCTL, (WPARAM)ctrl->ctlHwnd, TRUE);
	Sleep(FOCUSTIME);
	SendMessage(ctrl->ctlHwnd, WM_SETTEXT, 0, (LPARAM)(LPCTSTR)ctrl->ctlName);
	Sleep(STEPTIME);
}
