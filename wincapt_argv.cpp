/* Windows-Related shit */
#include "pch.h"

/* Macros */
#define BIT 1 <<
#define DEF_POLLTIME 1
#define DEF_EXITTIME 1200
#define DEF_OUTPUTFILENAME "007_wincapt.wc" 

/* includes */
#include <cstring>
#include <string>
#include <iostream>

/* Own includes */
#include "include/argh.h"
#include "include/wincapt_strs.h"
#include "include/wincapt_argv.h"

/* Enums & Structs */
enum ARGV_ERRROR {
	ARGV_ERR_ANY = 0xBB,
	ARGV_ERR_INPUT = 0xE0,
	ARG_ERR_KEYWORDS = 0xE1,
	ARGV_ERR_FLAG = 0xE6,
	ARGV_ERR_PARAMS = 0xE7,
	ARGV_ERR_JODEDOR = 0xEE,
	
	ARGV_WARN_INPUT = 0xD0,
	ARGV_WARN_OUTPUT = 0xD1,
	ARGV_WARN_KEYWORDS = 0xD2,
	ARGV_WARN_POLLT = 0xD3,
	ARGV_WARN_KILLT = 0xD4,
	ARGV_WARN_MATCHES = 0xD5
};

enum ARGV_FLAGS_PARAMS {
	ARGV_FP_VERBOSE = BIT 0,
	ARGV_FP_POLLTIME = BIT 1,
	ARGV_FP_KILLTIME = BIT 2,
	ARGV_FP_INPUTF = BIT 3,
	ARGV_FP_OUTPUTF = BIT 4,
	ARGV_FP_KEYWORDS = BIT 5,
	ARGV_FP_LOGGFILE = BIT 6,
	ARGV_FP_MATCHES = BIT 7
};

/* Prototypes */

/* Definitions */
WC_Argv::WC_Argv()
{
	this->Flags = 0;
	this->Mode = ARGV_MODE_UNSET;
	this->Matches = 0;
	this->Verbose = false;
	this->PollTime = DEF_POLLTIME;
	this->ExitTime = DEF_EXITTIME;
	memset(this->KeyWords, 0, INPUTBUFFSZ);
	memset(this->InputFile, 0, INPUTBUFFSZ);
	memset(this->OutputFile, 0, INPUTBUFFSZ);
	memset(this->Invalid, 0, INVALIDFLAGS);
	memset(this->LoggerFileName, 0, INPUTBUFFSZ);
	strcpy_s(this->OutputFile, INPUTBUFFSZ, DEF_OUTPUTFILENAME);
	strcpy_s(this->LoggerFileName, INPUTBUFFSZ, DEF_LOGGERFILENAME);
}

WC_Argv::~WC_Argv()
{

}

bool WC_Argv::CmdL(int argc, char **argv)
{
	int i;
	bool valid;
	JCString jcsObj;
	argh::parser cmdl;
	char auxInput[INPUTBUFFSZ];

	i = 0;
	valid = true;
	memset(auxInput, 0, INPUTBUFFSZ);
	cmdl.parse(argc, argv, argh::parser::PREFER_PARAM_FOR_UNREG_OPTION);

	/* Parsing flags */
	for (auto &it : cmdl.flags()) {
		if (it == "a") {
			if (this->Mode != ARGV_MODE_UNSET) {
				this->PrintErr(ARGV_ERR_JODEDOR);
				return (false);
			}
			this->Mode = ARGV_MODE_AUTONOMOUS;
		}
		else if (it == "g") {
			if (this->Mode != ARGV_MODE_UNSET) {
				this->PrintErr(ARGV_ERR_JODEDOR);
				return (false);
			}
			this->Mode = ARGV_MODE_GUIDED;
		}
		else if (it == "D") {
			this->Mode = ARGV_MODE_AUTONOMOUS_D;
		}
		else if (it == "v") {
			this->Verbose = true;
		}
		else if (it == "h") {
			this->PrintHelp(false);
			return (false);
		}
		else if (it == "H") {
			this->PrintHelp(true);
			return (false);
		} else {
			this->Invalid[i++] = *it.c_str();
			valid = false;
		}
	}
	if (!valid) {
		this->PrintErr(ARGV_ERR_FLAG);
		return (false);
	}

	/* Parsing parameters */
	for (auto &it : cmdl.params()) {
		if (it.first == "m") {
			try {
				this->Matches = std::stoi(it.second, NULL);
				this->Flags |= ARGV_FP_MATCHES;
			}
			catch (...) {
				printf("-m only accepts int values (default setted)\n");
			}
		}
		else if (it.first == "p") {
			try {
				this->PollTime = std::stoi(it.second, NULL);
				this->Flags |= ARGV_FP_POLLTIME;
			} catch (...) {
				printf("-p only accepts int values (default setted)\n");
			}
		}
		else if (it.first == "e") {
			try {
				this->ExitTime = std::stoi(it.second, NULL);
				this->Flags |= ARGV_FP_KILLTIME;
			} catch (...) {
				printf("-e only accepts int values (default setted)\n");
			}
		}
		else if (it.first == "I" || it.first == "i") {
			strcpy_s(this->InputFile, INPUTBUFFSZ, it.second.c_str());
			this->Flags |= ARGV_FP_INPUTF;
		}
		else if (it.first == "O" || it.first == "o") {
			strcpy_s(this->OutputFile, INPUTBUFFSZ, it.second.c_str());
			this->Flags |= ARGV_FP_OUTPUTF;
		}
		else if (it.first == "K" || it.first == "k") {
			strcpy_s(auxInput, INPUTBUFFSZ, it.second.c_str());
			jcsObj.CharToWchar(this->KeyWords, auxInput);
			this->Flags |= ARGV_FP_KEYWORDS;
		}
		else if (it.first == "L" || it.first == "l") {
			strcpy_s(this->LoggerFileName, INPUTBUFFSZ, it.second.c_str());
			this->Flags |= ARGV_FP_LOGGFILE;
		}
		else {
			this->Invalid[i++] = *it.first.c_str();
			valid = false;
		}
	}

	if (this->Mode == ARGV_MODE_AUTONOMOUS_D)
		return (true);
	if (!valid) {
		this->PrintErr(ARGV_ERR_PARAMS);
		return (false);
	}

	if (this->Mode == ARGV_MODE_UNSET)
		this->Mode = ARGV_MODE_GUIDED;

	/* Some extra checks */
	if (this->Mode == ARGV_MODE_AUTONOMOUS) {
		if (!(this->Flags & ARGV_FP_INPUTF)) {
			this->PrintErr(ARGV_ERR_INPUT);
			return (false);
		}
		if ((this->Flags & ARGV_FP_KEYWORDS))
			this->PrintErr(ARGV_WARN_KEYWORDS);
		
		if (this->Flags & ARGV_FP_OUTPUTF)
			this->PrintErr(ARGV_WARN_OUTPUT);

		if (this->Flags & ARGV_FP_MATCHES)
			this->PrintErr(ARGV_WARN_MATCHES);
	} else {
		if (!(this->Flags & ARGV_FP_KEYWORDS)) {
			this->PrintErr(ARG_ERR_KEYWORDS);
			return (false);
		}
		if (this->Flags & ARGV_FP_INPUTF)
			this->PrintErr(ARGV_WARN_INPUT);
		if (this->Flags & ARGV_FP_POLLTIME)
			this->PrintErr(ARGV_WARN_POLLT);
		if (this->Flags & ARGV_FP_KILLTIME)
			this->PrintErr(ARGV_WARN_KILLT);
	}
	return (true);
}

void WC_Argv::PrintHelp(bool ascii)
{
	printf("\nWincapt: To wincapture or not to wincapture!\n\n");
	printf("\nUsage: ./wincapt.exe [FLAGS] [PARAMETERS] VALUES\n");
	printf("\n\t-v\tVerbose (prints extra info)\n");
	printf("\n\t-g\tGuided-mode (default mode. -k needed)\n");
	printf("\n\t-a\tAutonomous-mode (execute steps from file. -i needed)\n");
	printf("\n\t-m\tMatch 'm' keywords\n");
	printf("\n\t-p\tPoll time (try to capture the window every 'p' seconds)\n");
	printf("\t\tWorks only in 'Autonomous mode'(Default: 2s)\n");
	printf("\n\t-e\tExit time (on -p failure abort at 'p' seconds)\n");
	printf("\t\tDefault: 1200s\n");
	
	printf("\n\t-i -I\tInput file\n");
	printf("\n\t-o -O\tOutput file (works only in 'Guided-mode'\n");
	printf("\t\tDefault name: 'wincapt.wc'\n");
	printf("\n\t-k -K\tKeywords/Window-name to capture\n");
	printf("\n\t-l -L\tLog file (default stdout)\n");
	printf("\n\t-h -H\tPrints this help message (-H prints ascii art)\n\n");
	if (ascii)
		this->PrintAsciiArt();
}

void WC_Argv::PrintErr(int err)
{
	size_t i, j;

	j = strlen(this->Invalid);
	switch (err) {
	case ARGV_ERR_FLAG:
		printf("Error: Invalid flags [");
		break;

	case ARGV_ERR_PARAMS:
		printf("Error: Invalid parameters [");
		break;

	case ARGV_ERR_INPUT:
		printf("Error: No input file provided\n");
		return;

	case ARG_ERR_KEYWORDS:
		printf("Error: Keywords or window name must be provided (-h for help)\n");
		return;

	case ARGV_ERR_JODEDOR:
		printf("Autonomo o guiado? *.*\n");
		return;

	case ARGV_WARN_INPUT:
		printf("Ignoring [-i, -I] (No input file needed for 'Guided-mode')\n");
		break;
	
	case ARGV_WARN_OUTPUT:
		printf("Ignoring [-o, -O] (No output file needed for 'Autonomous-mode')\n");
		break;
	
	case ARGV_WARN_KEYWORDS:
		printf("Ignoring [-k, -K] (No keywords needed for 'Autonomous-mode')\n");
		break;
	
	case ARGV_WARN_MATCHES:
		printf("Ignoring [-m] (No keyword matches needed for 'Autonomous-mode')\n");
		break;
	
	case ARGV_WARN_POLLT:
		printf("Ignoring [-p] (No poll time needed for 'Guided-mode')\n");
		break;

	case ARGV_WARN_KILLT:
		printf("Ignoring [-e] (No exit time needed for 'Guided-mode')\n");
		break;
	}

	for (i = 0; i < j; i++) {
		if (i != j - 1)
			printf("-%c, ", this->Invalid[i]);
		else
			printf("-%c]\n", this->Invalid[i]);
	}
}

void WC_Argv::PrintAsciiArt()
{
	printf("\n.-----------------------------------------------------------------------.\n");
	printf("| [Esc] [F1][F2][F3][F4][F5][F6][F7][F8][F9][F0][F10][F11][F12]  o o o  |\n");
	printf("| [`][1][2][3][4][5][6][7][8][9][0][-][=][_ < _] [I][H][U] [N][/][*][-] |\n");
	printf("| [| -][Q][W][E][R][T][Y][U][I][O][P][{][}]  | | [D][E][D] [7][8][9]|+| |\n");
	printf("| [CAP][A][S][D][F][G][H][J][K][L][;]['][#]|_|             [4][5][6]|_| |\n");
	printf("| [^][\\][Z][X][C][V][B][N][M][,][.][/] [__^__]      [^]    [1][2][3]| | |\n");
	printf("| [c]   [a][________________________][a]    [c]  [<][V][>] [ 0  ][.]|_| | \n");
	printf("`-----------------------------------------------------------------------'\n\n");
}

void WC_Argv::PrintCmds()
{
	std::cout << std::endl;
	switch (this->Mode) {
	case ARGV_MODE_AUTONOMOUS:
	case ARGV_MODE_AUTONOMOUS_D:
		std::cout << "Autonomo" << std::endl;
		break;
		
	case ARGV_MODE_GUIDED:
		std::cout << "Guiado" << std::endl;
		break;
	
	case ARGV_MODE_UNSET:
		std::cout << "UNSET???? NO NO" << std::endl;

	default:
		std::cout << "Este print no deberia ocurrir" << std::endl;
	}

	std::cout << "Verbose: " << this->Verbose << std::endl;
	std::cout << "Polltime: " << this->PollTime << std::endl;
	std::cout << "Killtime: " << this->ExitTime << std::endl;
	std::cout << "Input File: " << this->InputFile << std::endl;
	std::cout << "Output File: " << this->OutputFile << std::endl;
	std::wcout << "Keywords: " << this->KeyWords << std::endl;
	std::cout << "Log File: " << this->LoggerFileName << std::endl;
	std::cout << std::endl;

	int i = 0;
	while (this->Invalid[i] != 0)
		std::cout << "Invalid " << this->Invalid[i++] << std::endl;
}