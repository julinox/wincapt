//Wh@t1H@pp#n#d7T + 0FF9

/* C style includes */
#include "pch.h"
#include <stdio.h>
#include <windows.h>

/* C++ style includes */
#include <string>
#include <iostream>

/* Own includes */
#include "include/ClWindow.h"
#include "include/ClCommand.h"

/* Macros */
#define PRIVATE static
#define DEBUG(x) printf("DEBUG-%d\n", x);

/* Glocals */
PRIVATE const std::string Dlm = "/";
//PRIVATE LPCTSTR TargetName = L"Window_0xB0";
//PRIVATE LPCTSTR TargetName = L"DHCP Client Properties (Local Computer)";
PRIVATE LPCTSTR TargetName = L"Mozilla Thunderbird Setup";
//PRIVATE LPCTSTR TargetName = L"Mozilla Thunderbird Uninstall";

/* Enums & Structs */
enum ERRORS {
	ERR_NAMENOTFOUND = 0xA0
};

/* Prototypes */
void __main__(int);
void __Abort__(int, bool);
void __cycle__(int, ClCommand*);
int __parse__(std::string line, int *arr);
void __action__(ClWindow*, ClCommand*, int*, int);
const char* ErrPrint(int);

/********** TCHAR **********/
/*
	#ifdef _UNICODE
		typedef wchar_t TCHAR;
	#else
		typedef char TCHAR;
	#endif

*/


/* Definitions */
PRIVATE int TargetNameSz = 0;

typedef struct {
	int minSz;
	u_char kwMatches;
	LPCTSTR **keywords;
} NAME_MATCH;

BOOL CALLBACK FindWindowCustomCB(HWND hwnd, LPARAM lp)
{
	wchar_t auxBuff[512];

	memset(auxBuff, 0, 512 * sizeof(WCHAR));
	if (GetWindowText(hwnd, auxBuff, 512))
		wprintf(L"HWND: %p - *%s*\n", hwnd, auxBuff);
	return (true);
}


/* All the defines */
#define TOKENS_MAX 8
#define TOKEN_MAX_SZ 256
int GetTokens(wchar_t *target, wchar_t tokens[][TOKENS_MAX], wchar_t dlm)
{
	/*
	 * Fuck it, i'm going old school here (& not because i want to)
	 *
	 * PS: Given a string, split it into tokens
	 */

	int count;
	wchar_t *prev, *next;
	
	if (target == NULL)
		return 0;
	
	/* Getting keywords */
	count = 0;
	tokens = NULL;
	prev = next = target;

	while (*next != 0) {
		if (*next == dlm) {
			*next = 0;
			
			//
			wprintf(L"KW: *%s* C: %d\n", prev, count);
			//
			
			prev = ++next;
			count = 0;
			continue;
		}
		++next; ++count;
	}
	wprintf(L"KW: *%s* C: %d\n", prev, count);
	return count;
}
//EnumWindows(FindWindowCustomCB, );

int main()
{
	return 0;
}

int _main(int argc, char **argv)
{
	//__main__(1);
	wchar_t c = ' ';
	wchar_t tokens[TOKEN_MAX_SZ][TOKENS_MAX];
	wchar_t targetName[] = L"Mozilla Thunderbird Uninstall";
	
	GetTokens(targetName, tokens, c);
	return 0;
}

void __main__(int steps)
{
	int step;
	HWND target;
	ClCommand *cmd;
	
	target = FindWindow(NULL, TargetName);
	if (target == NULL) {
		printf("Error: %d\n", GetLastError());
		__Abort__(ERR_NAMENOTFOUND, true);
	}
	
	step = 1;
	cmd = new ClCommand(target);
	while (step <= steps) {
		__cycle__(step++, cmd);
		Sleep(STEPTIME);
	}
	delete cmd;
}

void __cycle__(int step, ClCommand *cmd)
{
	int tcSz;
	char auxBuf[64];
	ClWindow *window;
	int touchCtrls[TOUCHCTRLS];

	memset(auxBuf, 0, 64);
	window = new ClWindow();
	if (window == NULL) {
		printf("Oh Boy. This print is bad\n");
		return;
	}
	window->CtrlsCapture(cmd->parent);
	window->CtrlsPrint(CLASS_BUTTON | CLASS_EDIT, FIELD_ID | FIELD_SNAME | FIELD_NAME);
	
	std::cout << "Accion? ";
	std::cin >> auxBuf;
	tcSz = __parse__(auxBuf, touchCtrls);
	__action__(window, cmd, touchCtrls, tcSz);
	delete window;
}

void __action__(ClWindow *window, ClCommand *cmd, int *tCtrls, int tcSz)
{
	CTRL_DATA *ctrl;

	for (int i = 0; i < tcSz; i++) {
		ctrl = NULL;
		ctrl = window->CtrlsGetById(tCtrls[i]);
		if (ctrl == NULL) {
			printf("Capture fail id: %d\n", tCtrls[i]);
			continue;
		}
		cmd->CmdAction(ctrl);
	}
}

void __Abort__(int err, bool act)
{
	printf("%s\n", ErrPrint(err));
	if (act)
		_exit(-2);
}

int __parse__(std::string line, int *arr)
{
	int arrSz;
	size_t pos, dlmSz;
	std::string token;

	if (arr == NULL)
		return 0;
	pos = 0;
	arrSz = 0;
	dlmSz = Dlm.length();
	memset(arr, 0, TOUCHCTRLS);

	while ((pos = line.find(Dlm)) != std::string::npos) {
		token = line.substr(0, pos);
		arr[arrSz++] = std::stoi(token);
		line.erase(0, pos + dlmSz);
	}
	arr[arrSz++] = std::stoi(line);
	return (arrSz);
}

const char* ErrPrint(int err)
{
	/* Print a given error */
	switch (err) {
	case 0xA0:
		return ("Window name not found");
	default:
		return ("");
	}
}
