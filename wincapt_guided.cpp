/* Windows-related shit */
#include "pch.h"

/* Macros */
#define TEMPBUFSZ 256
#define _WC_GUIDED_RPARAMS_
#define _WC_GUIDED_PRIVATE_
#define FAIL_CAPTURE_WAIT 1250
#define LOG_THIS(x) printf("%s",x);

/* Enums & Structs */
enum ACTION_ {
	LOOP_CAPTURE = 0xA0,
	LOOP_PRINT = 0xA1,
	LOOP_READ = 0xA2,
	LOOP_SAVE = 0xA5,
	LOOP_SAVE_FINALIZE = 0xAE,
	LOOP_FINALIZE = 0xAF
};

/* Includes */
#include <queue>
#include <string>
#include <iostream>
#include <signal.h>
#include <Windows.h>

/* Own includes */
#include "include/wincapt_logger.h"
#include "include/wincapt_argv.h"
#include "include/wincapt_strs.h"
#include "include/wincapt_name.h"
#include "include/ClWindow.h"
#include "include/ClCommand.h"
#include "include/wincapt_step.h"
#include "include/wincapt_guided.h"

/* Glocals */
static bool SigBrk = 0;

/* Typedef */
typedef void(*SigHandler)(int);

/* Prototypes */
void CtrlBrk(int);

/* Definitions */
WC_Guided::WC_Guided()
{
	this->Delimiter = "/";
}

WC_Guided::~WC_Guided()
{
}

void WC_Guided::Run(WC_Argv *argvv, WC_Logger *logger)
{
	bool stop;
	STEP_H stph;
	SigHandler sigH;
	R_PARAMS rParams;

	/* Pendiente con rParams diablo blanco */
	stop = false;
	memset(&rParams, 0, sizeof(R_PARAMS));
	memset(&stph, 0, sizeof(STEP_H));
	rParams.argvv = argvv;
	rParams.logger = logger;

	rParams.name = new WC_Name(argvv->KeyWords, argvv->Matches, logger);
	rParams.steps = new WC_Step(rParams.argvv->OutputFile);
	
	/* Loop first step */
	rParams.loop = LOOP_CAPTURE;
	rParams.doWrite = true;
	rParams.stepNmb = 1;

	sigH = signal(SIGBREAK, CtrlBrk);
	while (rParams.loop != LOOP_FINALIZE && !SigBrk) {
		switch (rParams.loop) {

		case LOOP_CAPTURE:
			this->__Capture__(&rParams);
			break;

		case LOOP_PRINT:
			this->__Print__(&rParams);
			break;

		case LOOP_READ:
			this->__Read__(&rParams);
			break;

		case LOOP_SAVE:
		case LOOP_SAVE_FINALIZE:
			this->__Save__(&rParams);
			break;

		default:
			rParams.loop = LOOP_FINALIZE;
		}
	}
	if (SigBrk)
		goto __DEALLOC__;
	stph.matches = rParams.argvv->Matches;
	stph.pollTime = rParams.argvv->PollTime;
	stph.exitTime = rParams.argvv->ExitTime;
	if (rParams.doWrite) {
		rParams.steps->WriteInfo(rParams.argvv->LoggerFileName,
			rParams.name->Tokens, &stph);
		std::cout << "Done! Output file: %s" << rParams.argvv->OutputFile << std::endl;
	}

	__DEALLOC__:
	if (rParams.window != NULL)
		delete(rParams.window);
	delete(rParams.name);
	delete(rParams.steps);
}

void WC_Guided::__Capture__(R_PARAMS *rParams)
{
	if (rParams == NULL) {
		LOG_THIS("__Capture__(rParams = NULL)");
		exit(EXIT_FAILURE);
	}
	/*				CAMBIO
	if (rParams->window != NULL)
		delete(rParams->window);
	rParams->window = new ClWindow();
	*/
	rParams->name->FindWindowCustom();
	if (rParams->name->ActHwnd == NULL) {
		std::cout << "Could not capture window...\n";
		Sleep(FAIL_CAPTURE_WAIT);
		return;
	}
	/*            PENDIENTE ACA            */
	if (rParams->window != NULL)
		delete(rParams->window);
	rParams->window = new ClWindow();
	/*                                    */
	rParams->window->CtrlsCapture(rParams->name->ActHwnd);
	rParams->loop = LOOP_PRINT;
}

void WC_Guided::__Print__(R_PARAMS *rParams)
{
	if (rParams == NULL) {
		LOG_THIS("__Print__(rParams = NULL)");
		exit(EXIT_FAILURE);
	}
	if (rParams->argvv->Verbose)
		rParams->window->CtrlsPrint(CLASS_ANY, 0xFF);
	else
		rParams->window->CtrlsPrint(CLASS_BUTTON | CLASS_EDIT,
			FIELD_ID | FIELD_GLOBALID | FIELD_SNAME | FIELD_NAME);
	rParams->loop = LOOP_READ;
}

void WC_Guided::__Read__(R_PARAMS *rParams)
{
	size_t pos, dlmSz;
	std::string input, token;

	if (rParams == NULL) {
		LOG_THIS("__Read__(rParams = NULL)");
		exit(EXIT_FAILURE);
	}
	dlmSz = this->Delimiter.length();

	std::cout << "Accion? ";
	std::cin >> input;
	if (input == "f" || input == "F") {
		rParams->loop = LOOP_SAVE_FINALIZE;
		std::cout << "#&0xFF#: ";
		std::cin >> input;
	} else if (input == "x" || input == "X") {
		rParams->loop = LOOP_FINALIZE;
		rParams->doWrite = false;
		return;
	} else if (input == "d" || input == "D") {
		rParams->steps->DeleteLastStep();
		rParams->loop = LOOP_READ;
		printf("Done! ('r' for reload)\n");
		return;
	} else if (input == "r" || input == "R") {
		rParams->loop = LOOP_CAPTURE;
		return;
	}
	else if (input == "p" || input == "P") {
		rParams->steps->PrintSteps();
		rParams->loop = LOOP_READ;
		return;
	} 
	else if (input == "h" || input == "H" ||
			input == "-h" || input == "-H") {
		std::cout << "f: Final step\n";
		std::cout << "x: Quit\n";
		std::cout << "d: Delete last step\n";
		std::cout << "r: Reload controls capture\n";
		std::cout << "h: Print steps\n";
		std::cout << "h: Print this help\n";
	}
	else {
		rParams->loop = LOOP_SAVE;
	}

	rParams->noNameSz = 0;
	memset(rParams->noName, 0, TOUCHCTRLS);
	
	while ((pos = input.find(this->Delimiter)) != std::string::npos) {
		token = input.substr(0, pos);
		try {rParams->noName[rParams->noNameSz++] = std::stoi(token);}
		catch (...) { std::cout << "Invalid: " << token << std::endl; }
		input.erase(0, pos + dlmSz);
	}
	token = input.substr(0, pos);
	try { rParams->noName[rParams->noNameSz++] = std::stoi(token);}
	catch (...) { std::cout << "Invalid: " << token << std::endl;}
}

void WC_Guided::__Save__(R_PARAMS *rParams)
{	
	CTRL_DATA *ctrl;
	ClCommand clCmd;

	if (rParams == NULL) {
		LOG_THIS("__Save__(rParams = NULL)");
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < rParams->noNameSz; i++) {
		ctrl = rParams->window->CtrlsGetById(rParams->noName[i]);
		if (ctrl == NULL)
			continue;

		if (ctrl->ctlFlags & CLASS_EDIT) {
			memset(ctrl->ctlName, 0, TEMPBUFSZ);
			std::cout << "Text?#: ";
			std::wcin.ignore();
			std::wcin.getline(ctrl->ctlName, CTRL_NAME_SZ - 1);
		}

		if (rParams->loop == LOOP_SAVE)
			clCmd.CmdAction(ctrl);
		rParams->steps->Push(ctrl, rParams->window->winClassDist, rParams->stepNmb);
	}

	rParams->stepNmb++;
	if (rParams->loop == LOOP_SAVE_FINALIZE)
		rParams->loop = LOOP_FINALIZE;
	else
		rParams->loop = LOOP_CAPTURE;
}

void WC_Guided::SetDlm(std::string dlm)
{
	this->Delimiter = dlm;
}

void CtrlBrk(int signal)
{
	SigBrk = 1;
}