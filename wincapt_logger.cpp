/* Windows-Related shit */
#include "pch.h"

/* Macros */
#define LOGFILE_FALLBACK "log_fallback.wc"

/* C style includes */
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <windows.h>


/* C++ style includes */

/* Own includes */
#include "include/wincapt_logger.h"

/* Prototypes */

/* Definitions */


WC_Logger::WC_Logger(char *fPath)
{
	if (fPath == NULL || !_stricmp(fPath, "stdout")) {
		this->LogFile = stdout;
	} else {
		try {
			if (fopen_s(&this->LogFile, fPath, "a")) {
				fopen_s(&this->LogFile, LOGFILE_FALLBACK, "a");
			}
		} catch (...) {
			this->LogFile = stdout;
		}
	}
}


WC_Logger::~WC_Logger()
{
	if (this->LogFile != NULL && this->LogFile != stdout)
		fclose(this->LogFile);
}

void WC_Logger::Log(const wchar_t *fmt, ...)
{
	va_list ap;

	if (this->LogFile == NULL)
		return;
	this->LogTime();
	va_start(ap, fmt);
	vfwprintf(this->LogFile, fmt, ap);
	va_end(ap);
	fprintf(this->LogFile, "\n");
}

void WC_Logger::Log(const char *fmt, ...)
{
	va_list ap;
	
	if (this->LogFile == NULL)
		return;

	this->LogTime();
	va_start(ap, fmt);
	vfprintf(this->LogFile, fmt, ap);
	va_end(ap);
	fprintf(this->LogFile, "\n");
}

void WC_Logger::LogTime()
{
	SYSTEMTIME _time;
	memset(&_time, 0, sizeof(SYSTEMTIME));
	GetLocalTime(&_time);
	fprintf(this->LogFile, "[%d/%d/%d - %d:%d:%d] ",
		_time.wDay, _time.wMonth, _time.wYear,
		_time.wHour, _time.wMinute, _time.wSecond);
}

void WC_Logger::LogErr(int err, void *ptr)
{

}

void WC_Logger::LogErr(int err, const wchar_t *fmt, ...)
{

}