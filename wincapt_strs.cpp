/* Windows-related shit */
#include "pch.h"

/* Macros */
#define AUXBUFSZ 256
#define TOKEN_MAX_SZ 256

/* Includes */
#include <string>
#include <iostream>

/* Own includes */
#include "include/wincapt_strs.h"

JCString::JCString()
{
	this->CharTrim = ' ';
	this->WcharTrim = L' ';
}

JCString::JCString(char cTrim)
{
	this->CharTrim = cTrim;
	this->WcharTrim = L' ';
}

JCString::JCString(wchar_t wTrim)
{
	this->CharTrim = ' ';
	this->WcharTrim = wTrim;
}

JCString::~JCString()
{
}

size_t JCString::GetSize(const char *str)
{
	return (str == NULL ? 0 : strlen(str));
}

size_t JCString::GetSize(const wchar_t *str)
{
	return (str == NULL ? 0 : wcslen(str));
}

char *JCString::TrimL(char *src)
{
	size_t count, srcLen;

	if (src == NULL)
		return (NULL);
	count = 0;
	srcLen = strlen(src);
	while (count < srcLen && *(src + count) == this->CharTrim)
		count++;
	return (src + count);
}

wchar_t *JCString::TrimL(wchar_t *src)
{
	size_t count, srcLen;

	if (src == NULL)
		return (NULL);
	count = 0;
	srcLen = wcslen(src);
	while (count < srcLen && *(src + count) == this->WcharTrim)
		count++;
	return (src + count);
}

void JCString::TrimR(char *src)
{
	char *ptr;
	size_t srcLen, count;

	if (src == NULL)
		return;
	count = 0;
	srcLen = strlen(src);
	ptr = src + (srcLen - 1);
	while (ptr > src && *ptr == this->CharTrim) {
		--ptr;
		count++;
	}
	*(ptr + 1) = 0;
}

void JCString::TrimR(wchar_t *src)
{
	wchar_t *ptr;
	size_t srcLen, count;

	if (src == NULL)
		return;
	count = 0;
	srcLen = wcslen(src);
	ptr = src + (srcLen - 1);
	while (ptr > src && *ptr == this->WcharTrim) {
		--ptr;
		count++;
	}
	*(ptr + 1) = 0;
}

void JCString::Trim(char *dst, size_t dstLen, char *src)
{
	char *ptr;

	if (src == NULL || dst == NULL)
		return;
	ptr = this->TrimL(src);
	strcpy_s(dst, dstLen, ptr);
	this->TrimR(dst);
}

void JCString::Trim(wchar_t *dst, size_t dstLen, wchar_t *src)
{
	wchar_t *ptr;

	if (src == NULL || dst == NULL)
		return;
	ptr = this->TrimL(src);
	wcscpy_s(dst, dstLen, ptr);
	this->TrimR(dst);
}

void JCString::Tokenize(CTOKENS *dst, char *src, const char *dlm)
{
	/* Be carefull: This functions modifys src buffer */

	char *buffer;
	char *actualToken, *nextToken;

	if (src == NULL || dst == NULL)
		return;
	
	actualToken = strtok_s(src, dlm, &nextToken);
	while (actualToken != NULL) {
		buffer = new char[TOKEN_MAX_SZ];
		if (buffer != NULL) {
			memset(buffer, 0, TOKEN_MAX_SZ);
			strcpy_s(buffer, TOKEN_MAX_SZ, actualToken);
			dst->push_back(buffer);
		}
		actualToken = strtok_s(NULL, dlm, &nextToken);
	}
}

void JCString::Tokenize(WTOKENS *dst, wchar_t *src, const wchar_t *dlm)
{
	/* Be carefull: This functions modifys src buffer */
	
	wchar_t *buffer;
	wchar_t *actualToken, *nextToken;

	if (src == NULL || dst == NULL)
		return;
	actualToken = wcstok_s(src, dlm, &nextToken);
	while (actualToken != NULL) {
		buffer = new wchar_t[TOKEN_MAX_SZ];
		if (buffer != NULL) {
			memset(buffer, 0, TOKEN_MAX_SZ * sizeof(wchar_t));
			wcscpy_s(buffer, TOKEN_MAX_SZ, actualToken);
			dst->push_back(buffer);
		}
		actualToken = wcstok_s(NULL, dlm, &nextToken);
	}
}

void JCString::CharToWchar(wchar_t *dst, char *src)
{
	char aux[AUXBUFSZ];

	if (dst == NULL || src == NULL)
		return;

	memset(aux, 0, AUXBUFSZ);
	this->Trim(aux, AUXBUFSZ, src);
	for (size_t i = 0; i < strlen(aux); i++)
		mbtowc(dst + i, aux + i, sizeof(char));
}

void JCString::PrintTokens(CTOKENS *tks)
{
	CTOKENSIT it;

	if (tks == NULL)
		return;
	it = tks->begin();
	for (; it != tks->end(); ++it)
		std::cout << "|" << *it << "| ";
	std::cout << std::endl;
}

void JCString::PrintTokens(WTOKENS *tks)
{
	WTOKENSIT it;

	if (tks == NULL)
		return;
	it = tks->begin();
	for (; it != tks->end(); ++it)
		std::wcout << "|" << *it << "|";
	std::cout << std::endl;
}

void JCString::ReleaseTokens(CTOKENS *tks)
{
	CTOKENSIT it;

	if (tks == NULL)
		return;
	for (it = tks->begin(); it != tks->end(); ++it)
		delete(*it);

}

void JCString::ReleaseTokens(WTOKENS *tks)
{
	WTOKENSIT it;

	if (tks == NULL)
		return;
	for (it = tks->begin(); it != tks->end(); ++it)
		delete(*it);
}