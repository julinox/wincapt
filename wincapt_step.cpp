/* Windows-related shit */
#include "pch.h"

/* Macros */
#define TEMPBUFSZ 256
#define STEPITERATOR std::list<_STEP_*>::iterator

/* Includes */
#include <list>
#include <stdio.h>
#include <windows.h>

/* Own includes */
#include "include/wincapt_logger.h"
#include "include/wincapt_strs.h"
#include "include/wincapt_step.h"

/* Enums & Structs */
WC_Step::WC_Step(char *fileName)
{
	this->FilePtr = NULL;
	this->Logger = NULL;
	this->FileName = fileName;
}

WC_Step::~WC_Step()
{
	std::list<_STEP_*>::iterator it;

	it = this->Steps.begin();
	for (; it != this->Steps.end(); ++it)
		delete(*it);
	this->Steps.clear();
	this->FilePtr = NULL;
	this->Logger = NULL;
}

void WC_Step::Push(CTRL_DATA *ctrl, u_int winClassDist, int step)
{
	_STEP_ *newStep;

	if (ctrl == NULL)
		return;
	newStep = new _STEP_;
	memset(newStep, 0, sizeof(_STEP_));
	newStep->ctrl.ctlID = ctrl->ctlID;
	newStep->ctrl.ctlHwnd = ctrl->ctlHwnd;
	newStep->ctrl.globalID = ctrl->globalID;
	newStep->ctrl.ctlFlags = ctrl->ctlFlags;
	
	strcpy_s(newStep->ctrl.ctlClassName, CTRL_NAME_SZ, ctrl->ctlClassName);
	strcpy_s(newStep->ctrl.ctlStyleName, CTRL_NAME_SZ, ctrl->ctlStyleName);
	wcscpy_s(newStep->ctrl.ctlName, CTRL_NAME_SZ, ctrl->ctlName);
	newStep->winClassDist = winClassDist;
	newStep->step = step;
	this->Steps.push_back(newStep);
}

void WC_Step::WriteInfo(char *loggerFname, WTOKENS &tks, STEP_H *stph)
{
	this->WriteHeading();
	this->WriteKeywords(tks);
	this->WriteLoggerFname(loggerFname);
	this->WriteOthers(stph);
	this->WriteSteps();
}

void WC_Step::DeleteLastStep()
{
	_STEP_ *last;
	
	if (this->Steps.empty()) 
		return;
	last = this->Steps.back();
	delete(last);
	this->Steps.pop_back();
}

void WC_Step::PrintSteps()
{
	STEPITERATOR it;

	it = this->Steps.begin();
	for (; it != this->Steps.end(); ++it) {
		printf("--------------------------------------------------------\n");
		printf("Id: %d\n", (*it)->ctrl.ctlID);
		printf("Global Id: %lu\n", (*it)->ctrl.globalID);
		printf("Hwnd: %p\n", (*it)->ctrl.ctlHwnd);
		printf("Flags: %d\n", (*it)->ctrl.ctlFlags);
		printf("Class Name: %s\n", (*it)->ctrl.ctlClassName);
		printf("Style Name: %s\n", (*it)->ctrl.ctlStyleName);
		wprintf(L"Ctrl Name: |%s|\n", (*it)->ctrl.ctlName);
		printf("--------------------------------------------------------\n");
	}
}

/* Privates */
void WC_Step::OpenFile(bool ft)
{
	size_t s;

	if (ft)
		s = fopen_s(&this->FilePtr, this->FileName, "w");
	else
		s = fopen_s(&this->FilePtr, this->FileName, "a");
	if (this->FilePtr == NULL || s) {
		printf("Error opening file (%d)\n", GetLastError());
		exit(EXIT_FAILURE);
	}
}


void WC_Step::WriteHeading()
{
	this->OpenFile(true);
	fprintf(this->FilePtr, "#\n# .exe MD5|SHA256: ???\n#\n\n");
	fclose(this->FilePtr);
}

void WC_Step::WriteKeywords(WTOKENS &tks)
{
	WTOKENSIT it;
	wchar_t *last;

	this->OpenFile(false);
	if (this->FilePtr == NULL)
		return;

	it = tks.begin();
	last = tks.back();
	fwprintf(this->FilePtr, L"# Keywords\n");
	for (; it != tks.end(); ++it) {
		if (*it != last)
			fwprintf(this->FilePtr, L"%s ", *it);
		else
			fwprintf(this->FilePtr, L"%s", *it);
	}
	fwprintf_s(this->FilePtr, L"\n\n");
	fclose(this->FilePtr);
}

void WC_Step::WriteLoggerFname(char *loggerFname)
{
	JCString jcs;
	wchar_t aux[TEMPBUFSZ];
	
	this->OpenFile(false);
	if (this->FilePtr == NULL)
		return;
	memset(aux, 0, sizeof(aux));
	jcs.CharToWchar(aux, loggerFname);
	fwprintf_s(this->FilePtr, L"# Log File Name\n%s\n\n", aux);
	fclose(this->FilePtr);
}

void WC_Step::WriteOthers(STEP_H *stph)
{
	this->OpenFile(false);
	fwprintf_s(this->FilePtr, L"# Matches | PollTime | ExitTime\n");
	fwprintf_s(this->FilePtr, L"%d %d %d\n\n", stph->matches, stph->pollTime,
		stph->exitTime);
	fclose(this->FilePtr);
}

void WC_Step::WriteSteps()
{
	std::list<_STEP_*>::iterator it;

	this->OpenFile(false);
	if (this->FilePtr == NULL)
		return;

	fwprintf_s(this->FilePtr, L"# Steps\n");
	it = this->Steps.begin();
	for (; it != this->Steps.end(); ++it) {
		fwprintf_s(this->FilePtr, L"<STEP>\n%d\n%d\n%d\n%d\n%s\n%d\n</STEP>\n\n",
			(*it)->step,(*it)->ctrl.ctlID, (*it)->ctrl.globalID, (*it)->ctrl.ctlFlags,
			(*it)->ctrl.ctlName, (*it)->winClassDist);
	}
	fclose(this->FilePtr);
}
