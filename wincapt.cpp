/* Windows-Related shit */
#include "pch.h"

/* Macros */
#define AUXBUFSZ__ 2048

/* C style includes */
#include <vld.h>
#include <string>
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <tlhelp32.h>

/* Own includes */
#include "include/wincapt_logger.h"
#include "include/wincapt_argv.h"
#include "include/wincapt_strs.h"
#include "include/wincapt_name.h"
#include "include/ClWindow.h"
#include "include/wincapt_guided.h"
#include "include/wincapt_step.h"
#include "include/wincapt_auto.h"

/* Glocals */

/* Prototypes */
void ReLaunch(int, char**, PROCESS_INFORMATION*);

/* Definitions */
int main(int argc, char **argv)
{
	int _ret;
	WC_Argv *argvv;
	WC_Guided gMode;
	WC_Logger *logger;
	WC_Autonomous aMode;
	PROCESS_INFORMATION pInfo;

	/* Todos los buffers son 256 * data_type_sz */
	_ret = 0;
	logger = NULL;
	argvv = new WC_Argv();
	if (!argvv->CmdL(argc, argv)) {
		_ret = -1;
		goto _DEALLOC_;
	}

	switch (argvv->Mode) {
	case ARGV_MODE_GUIDED:
		logger = new WC_Logger(argvv->LoggerFileName);
		//argvv->PrintCmds();
		gMode.Run(argvv, logger);
		break;

	case ARGV_MODE_AUTONOMOUS_D:
		aMode.Run(argvv);
		break;

	case ARGV_MODE_AUTONOMOUS:
		ZeroMemory(&pInfo, sizeof(pInfo));
		ReLaunch(argc, argv, &pInfo);
		WaitForSingleObject(pInfo.hProcess, 0);
		CloseHandle(pInfo.hProcess);
		CloseHandle(pInfo.hThread);
		break;
	}

_DEALLOC_:
	if (logger != NULL)
		delete(logger);
	if (argvv != NULL)
		delete(argvv);
	return (_ret);
}

void ReLaunch(int argc, char **argvv, PROCESS_INFORMATION *pInfo)
{
	BOOL ret;
	STARTUPINFOA stInfo;
	char cAuxC[AUXBUFSZ__];
	std::string newArgv, aux;

	newArgv = "wincapt.exe ";
	ZeroMemory(&stInfo, sizeof(stInfo));
	stInfo.cb = sizeof(stInfo);

	for (int i = 1; i < argc; i++) {
		aux = argvv[i];
		if (aux == "-a")
			newArgv.append("-D");
		else
			newArgv.append(argvv[i]);
		if (i < argc - 1)
			newArgv.append(" ");
	}
	memset(cAuxC, 0, AUXBUFSZ__);
	strcpy_s(cAuxC, AUXBUFSZ__, newArgv.c_str());
	ret = CreateProcessA(argvv[0], cAuxC, NULL, NULL, FALSE, 0, NULL, NULL,
		&stInfo, pInfo);
	if (!ret) {
		printf("LOG_THIS_SHIT(EMERGENCY)%d\n", GetLastError());
		exit(EXIT_FAILURE);
	}
}
